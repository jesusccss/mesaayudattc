﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MesaAyudaTTC{

    public partial class frmNotificacion : Form{
        public frmNotificacion(){
            InitializeComponent();
        }// end inicializar

        private void frmNotificacion_Load(object sender, EventArgs e){


            // ubicar la notificacion
            int espacio = 10;
            this.Left = Screen.AllScreens[0].Bounds.Width - this.Width - espacio; // ubica la ventana a la derecha de la pantalla
            //this.Top = (Screen.AllScreens[0].Bounds.Height - this.Height) - 50; // ubicar cerca de iconos
            this.Top = 0 + espacio;//(Screen.AllScreens[0].Bounds.Height - this.Height) - 50; // ubicar cerca de iconos


        }// end load

        private void btnCerrar_notificacion_Click(object sender, EventArgs e){
            this.Close();
        }// end cerrar notificacion

        public void Mostrar(string s) {
            lblTitulo.Text = s;

            txtMensaje.Text = "" ;

        }// end mostrar notificacion

        private void btnCerrar_notificacion_MouseHover(object sender, EventArgs e)
        {

        }

        private void btnCerrar_notificacion_MouseLeave(object sender, EventArgs e)
        {
            try{
                string ruta = System.IO.Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
                btnCerrar_notificacion.Image = almacen_img.Images[0];

            }catch (Exception)
            {
                // no hacer nada
            }
        }

        private void btnCerrar_notificacion_MouseEnter(object sender, EventArgs e)
        {
            try
            {
                string ruta = System.IO.Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
                btnCerrar_notificacion.Image = almacen_img.Images[1];

            }
            catch (Exception)
            {
                // no hacer nada
            }
        }
    }// end class
}// end namespace
