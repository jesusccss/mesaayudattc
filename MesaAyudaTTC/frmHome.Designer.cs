﻿namespace MesaAyudaTTC
{
    partial class frmHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHome));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.panelPercance = new System.Windows.Forms.TabPage();
            this.txtPercanceMotivo = new System.Windows.Forms.ComboBox();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.txtPercanceDetalle = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panelPendientes = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panelFlujo = new System.Windows.Forms.FlowLayoutPanel();
            this.lblPercanceModelo = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblIp = new System.Windows.Forms.Label();
            this.lblEquipo = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ubicacion = new System.Windows.Forms.Timer(this.components);
            this.iconizarApp = new System.Windows.Forms.NotifyIcon(this.components);
            this.menuContextual = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mostrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minimizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbl04 = new System.Windows.Forms.Label();
            this.lbl03 = new System.Windows.Forms.Label();
            this.lblNumeroEmpleado = new System.Windows.Forms.Label();
            this.lblNombreEmpleado = new System.Windows.Forms.Label();
            this.panelEmpleado = new System.Windows.Forms.Panel();
            this.btnValidarEmpleado = new System.Windows.Forms.Button();
            this.txtNumeroEmpleado = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.menuHome = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.itemTecnico = new System.Windows.Forms.ToolStripMenuItem();
            this.itemMinimizar = new System.Windows.Forms.ToolStripMenuItem();
            this.panelTecnico = new System.Windows.Forms.Panel();
            this.btnValidarTecnico = new System.Windows.Forms.Button();
            this.txtPassTecnico = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.CmbUsuarios = new System.Windows.Forms.ComboBox();
            this.tabControl1.SuspendLayout();
            this.panelPercance.SuspendLayout();
            this.panelPendientes.SuspendLayout();
            this.panelFlujo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuContextual.SuspendLayout();
            this.panelEmpleado.SuspendLayout();
            this.menuHome.SuspendLayout();
            this.panelTecnico.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.panelPercance);
            this.tabControl1.Controls.Add(this.panelPendientes);
            this.tabControl1.Location = new System.Drawing.Point(2, 150);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(343, 516);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // panelPercance
            // 
            this.panelPercance.Controls.Add(this.txtPercanceMotivo);
            this.panelPercance.Controls.Add(this.btnRegistrar);
            this.panelPercance.Controls.Add(this.txtPercanceDetalle);
            this.panelPercance.Controls.Add(this.label5);
            this.panelPercance.Controls.Add(this.label4);
            this.panelPercance.Location = new System.Drawing.Point(4, 22);
            this.panelPercance.Name = "panelPercance";
            this.panelPercance.Padding = new System.Windows.Forms.Padding(3);
            this.panelPercance.Size = new System.Drawing.Size(335, 490);
            this.panelPercance.TabIndex = 0;
            this.panelPercance.Text = "Percance";
            this.panelPercance.UseVisualStyleBackColor = true;
            // 
            // txtPercanceMotivo
            // 
            this.txtPercanceMotivo.FormattingEnabled = true;
            this.txtPercanceMotivo.Items.AddRange(new object[] {
            "Computadora",
            "Impresora",
            "Internet",
            "Programa",
            "Base",
            "Outlook",
            "Compañero"});
            this.txtPercanceMotivo.Location = new System.Drawing.Point(6, 20);
            this.txtPercanceMotivo.Name = "txtPercanceMotivo";
            this.txtPercanceMotivo.Size = new System.Drawing.Size(326, 21);
            this.txtPercanceMotivo.TabIndex = 18;
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Enabled = false;
            this.btnRegistrar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnRegistrar.Location = new System.Drawing.Point(161, 455);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(165, 29);
            this.btnRegistrar.TabIndex = 17;
            this.btnRegistrar.Text = "Notificar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            // 
            // txtPercanceDetalle
            // 
            this.txtPercanceDetalle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPercanceDetalle.Font = new System.Drawing.Font("Arial", 8.25F);
            this.txtPercanceDetalle.Location = new System.Drawing.Point(2, 61);
            this.txtPercanceDetalle.Name = "txtPercanceDetalle";
            this.txtPercanceDetalle.Size = new System.Drawing.Size(330, 388);
            this.txtPercanceDetalle.TabIndex = 16;
            this.txtPercanceDetalle.Text = "";
            this.txtPercanceDetalle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.richTextBox1_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 8.25F);
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(6, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 14);
            this.label5.TabIndex = 15;
            this.label5.Text = "Detalles:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 8.25F);
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(3, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 14);
            this.label4.TabIndex = 15;
            this.label4.Text = "Motivo:";
            // 
            // panelPendientes
            // 
            this.panelPendientes.Controls.Add(this.label6);
            this.panelPendientes.Controls.Add(this.button1);
            this.panelPendientes.Controls.Add(this.panelFlujo);
            this.panelPendientes.Location = new System.Drawing.Point(4, 22);
            this.panelPendientes.Name = "panelPendientes";
            this.panelPendientes.Padding = new System.Windows.Forms.Padding(3);
            this.panelPendientes.Size = new System.Drawing.Size(335, 490);
            this.panelPendientes.TabIndex = 1;
            this.panelPendientes.Text = "Pendientes";
            this.panelPendientes.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(9, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(317, 23);
            this.label6.TabIndex = 2;
            this.label6.Text = "Pencances pendientes:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(237, 461);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // panelFlujo
            // 
            this.panelFlujo.Controls.Add(this.lblPercanceModelo);
            this.panelFlujo.Location = new System.Drawing.Point(6, 28);
            this.panelFlujo.Name = "panelFlujo";
            this.panelFlujo.Size = new System.Drawing.Size(323, 428);
            this.panelFlujo.TabIndex = 0;
            // 
            // lblPercanceModelo
            // 
            this.lblPercanceModelo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPercanceModelo.Location = new System.Drawing.Point(3, 0);
            this.lblPercanceModelo.Name = "lblPercanceModelo";
            this.lblPercanceModelo.Size = new System.Drawing.Size(317, 50);
            this.lblPercanceModelo.TabIndex = 0;
            this.lblPercanceModelo.Text = "Linea 01\r\nLinea 02\r\nLinea 03";
            this.lblPercanceModelo.Visible = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox2.Location = new System.Drawing.Point(12, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(96, 89);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 12;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox1.Location = new System.Drawing.Point(152, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(183, 75);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // lblIp
            // 
            this.lblIp.AutoSize = true;
            this.lblIp.Font = new System.Drawing.Font("Arial", 8.25F);
            this.lblIp.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblIp.Location = new System.Drawing.Point(47, 685);
            this.lblIp.Name = "lblIp";
            this.lblIp.Size = new System.Drawing.Size(25, 14);
            this.lblIp.TabIndex = 14;
            this.lblIp.Text = "lblIp";
            // 
            // lblEquipo
            // 
            this.lblEquipo.AutoSize = true;
            this.lblEquipo.Font = new System.Drawing.Font("Arial", 8.25F);
            this.lblEquipo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblEquipo.Location = new System.Drawing.Point(47, 669);
            this.lblEquipo.Name = "lblEquipo";
            this.lblEquipo.Size = new System.Drawing.Size(49, 14);
            this.lblEquipo.TabIndex = 15;
            this.lblEquipo.Text = "lblequipo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 8.25F);
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(30, 685);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 14);
            this.label2.TabIndex = 16;
            this.label2.Text = "Ip:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 8.25F);
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(6, 669);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 14);
            this.label1.TabIndex = 17;
            this.label1.Text = "Equipo:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 28F);
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(42, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(277, 43);
            this.label3.TabIndex = 18;
            this.label3.Text = "Mesa de Ayuda";
            // 
            // ubicacion
            // 
            this.ubicacion.Enabled = true;
            this.ubicacion.Tick += new System.EventHandler(this.ubicacion_Tick);
            // 
            // iconizarApp
            // 
            this.iconizarApp.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.iconizarApp.BalloonTipText = "Mesa de ayuda - text";
            this.iconizarApp.BalloonTipTitle = "Mesa de ayuda - titulo";
            this.iconizarApp.Icon = ((System.Drawing.Icon)(resources.GetObject("iconizarApp.Icon")));
            this.iconizarApp.Text = "notifyIcon1";
            this.iconizarApp.BalloonTipClicked += new System.EventHandler(this.iconizarApp_BalloonTipClicked_1);
            // 
            // menuContextual
            // 
            this.menuContextual.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mostrarToolStripMenuItem,
            this.cerrarToolStripMenuItem,
            this.minimizarToolStripMenuItem});
            this.menuContextual.Name = "menuContextual";
            this.menuContextual.Size = new System.Drawing.Size(128, 70);
            // 
            // mostrarToolStripMenuItem
            // 
            this.mostrarToolStripMenuItem.Name = "mostrarToolStripMenuItem";
            this.mostrarToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.mostrarToolStripMenuItem.Text = "Mostrar";
            this.mostrarToolStripMenuItem.Click += new System.EventHandler(this.mostrarToolStripMenuItem_Click);
            // 
            // cerrarToolStripMenuItem
            // 
            this.cerrarToolStripMenuItem.Name = "cerrarToolStripMenuItem";
            this.cerrarToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.cerrarToolStripMenuItem.Text = "Cerrar";
            this.cerrarToolStripMenuItem.Click += new System.EventHandler(this.cerrarToolStripMenuItem_Click);
            // 
            // minimizarToolStripMenuItem
            // 
            this.minimizarToolStripMenuItem.Name = "minimizarToolStripMenuItem";
            this.minimizarToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.minimizarToolStripMenuItem.Text = "Minimizar";
            this.minimizarToolStripMenuItem.Click += new System.EventHandler(this.minimizarToolStripMenuItem_Click_1);
            // 
            // lbl04
            // 
            this.lbl04.AutoSize = true;
            this.lbl04.Font = new System.Drawing.Font("Arial", 8.25F);
            this.lbl04.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbl04.Location = new System.Drawing.Point(179, 685);
            this.lbl04.Name = "lbl04";
            this.lbl04.Size = new System.Drawing.Size(47, 14);
            this.lbl04.TabIndex = 15;
            this.lbl04.Text = "Nombre:";
            this.lbl04.Visible = false;
            // 
            // lbl03
            // 
            this.lbl03.AutoSize = true;
            this.lbl03.Font = new System.Drawing.Font("Arial", 8.25F);
            this.lbl03.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbl03.Location = new System.Drawing.Point(179, 669);
            this.lbl03.Name = "lbl03";
            this.lbl03.Size = new System.Drawing.Size(47, 14);
            this.lbl03.TabIndex = 15;
            this.lbl03.Text = "Numero:";
            this.lbl03.Visible = false;
            // 
            // lblNumeroEmpleado
            // 
            this.lblNumeroEmpleado.AutoSize = true;
            this.lblNumeroEmpleado.Font = new System.Drawing.Font("Arial", 8.25F);
            this.lblNumeroEmpleado.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblNumeroEmpleado.Location = new System.Drawing.Point(231, 669);
            this.lblNumeroEmpleado.Name = "lblNumeroEmpleado";
            this.lblNumeroEmpleado.Size = new System.Drawing.Size(63, 14);
            this.lblNumeroEmpleado.TabIndex = 15;
            this.lblNumeroEmpleado.Text = "lblEmpleado";
            this.lblNumeroEmpleado.Visible = false;
            // 
            // lblNombreEmpleado
            // 
            this.lblNombreEmpleado.AutoSize = true;
            this.lblNombreEmpleado.Font = new System.Drawing.Font("Arial", 8.25F);
            this.lblNombreEmpleado.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblNombreEmpleado.Location = new System.Drawing.Point(231, 685);
            this.lblNombreEmpleado.Name = "lblNombreEmpleado";
            this.lblNombreEmpleado.Size = new System.Drawing.Size(54, 14);
            this.lblNombreEmpleado.TabIndex = 15;
            this.lblNombreEmpleado.Text = "lblNombre";
            this.lblNombreEmpleado.Visible = false;
            // 
            // panelEmpleado
            // 
            this.panelEmpleado.Controls.Add(this.CmbUsuarios);
            this.panelEmpleado.Controls.Add(this.btnValidarEmpleado);
            this.panelEmpleado.Controls.Add(this.txtNumeroEmpleado);
            this.panelEmpleado.Controls.Add(this.label7);
            this.panelEmpleado.Location = new System.Drawing.Point(351, 172);
            this.panelEmpleado.Name = "panelEmpleado";
            this.panelEmpleado.Size = new System.Drawing.Size(345, 525);
            this.panelEmpleado.TabIndex = 20;
            // 
            // btnValidarEmpleado
            // 
            this.btnValidarEmpleado.Enabled = false;
            this.btnValidarEmpleado.Location = new System.Drawing.Point(180, 149);
            this.btnValidarEmpleado.Name = "btnValidarEmpleado";
            this.btnValidarEmpleado.Size = new System.Drawing.Size(150, 30);
            this.btnValidarEmpleado.TabIndex = 16;
            this.btnValidarEmpleado.Text = "Ingresar";
            this.btnValidarEmpleado.UseVisualStyleBackColor = true;
            this.btnValidarEmpleado.Click += new System.EventHandler(this.btnIngresarEmpleado_Click);
            // 
            // txtNumeroEmpleado
            // 
            this.txtNumeroEmpleado.Font = new System.Drawing.Font("Arial", 18F);
            this.txtNumeroEmpleado.Location = new System.Drawing.Point(13, 108);
            this.txtNumeroEmpleado.Name = "txtNumeroEmpleado";
            this.txtNumeroEmpleado.Size = new System.Drawing.Size(291, 35);
            this.txtNumeroEmpleado.TabIndex = 0;
            this.txtNumeroEmpleado.TextChanged += new System.EventHandler(this.txtNumeroEmpleado_TextChanged);
            this.txtNumeroEmpleado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumeroEmpleado_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 8.25F);
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(10, 91);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 14);
            this.label7.TabIndex = 15;
            this.label7.Text = "Numero de Empleado:";
            // 
            // menuHome
            // 
            this.menuHome.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemTecnico,
            this.itemMinimizar});
            this.menuHome.Name = "menuHome";
            this.menuHome.Size = new System.Drawing.Size(116, 48);
            // 
            // itemTecnico
            // 
            this.itemTecnico.Name = "itemTecnico";
            this.itemTecnico.Size = new System.Drawing.Size(115, 22);
            this.itemTecnico.Text = "Tecnico";
            this.itemTecnico.Click += new System.EventHandler(this.itemTecnico_Click);
            // 
            // itemMinimizar
            // 
            this.itemMinimizar.Name = "itemMinimizar";
            this.itemMinimizar.Size = new System.Drawing.Size(115, 22);
            this.itemMinimizar.Text = "Ocultar";
            this.itemMinimizar.Click += new System.EventHandler(this.itemMinimizar_Click);
            // 
            // panelTecnico
            // 
            this.panelTecnico.Controls.Add(this.btnValidarTecnico);
            this.panelTecnico.Controls.Add(this.txtPassTecnico);
            this.panelTecnico.Controls.Add(this.label8);
            this.panelTecnico.Location = new System.Drawing.Point(702, 172);
            this.panelTecnico.Name = "panelTecnico";
            this.panelTecnico.Size = new System.Drawing.Size(345, 525);
            this.panelTecnico.TabIndex = 20;
            // 
            // btnValidarTecnico
            // 
            this.btnValidarTecnico.Enabled = false;
            this.btnValidarTecnico.Location = new System.Drawing.Point(180, 149);
            this.btnValidarTecnico.Name = "btnValidarTecnico";
            this.btnValidarTecnico.Size = new System.Drawing.Size(150, 30);
            this.btnValidarTecnico.TabIndex = 16;
            this.btnValidarTecnico.Text = "Ingresar";
            this.btnValidarTecnico.UseVisualStyleBackColor = true;
            this.btnValidarTecnico.Click += new System.EventHandler(this.btnIngresarEmpleado_Click);
            // 
            // txtPassTecnico
            // 
            this.txtPassTecnico.Font = new System.Drawing.Font("Arial", 18F);
            this.txtPassTecnico.Location = new System.Drawing.Point(13, 108);
            this.txtPassTecnico.Name = "txtPassTecnico";
            this.txtPassTecnico.Size = new System.Drawing.Size(317, 35);
            this.txtPassTecnico.TabIndex = 0;
            this.txtPassTecnico.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPassTecnico_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 8.25F);
            this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label8.Location = new System.Drawing.Point(10, 91);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 15;
            this.label8.Text = "Tecnico:";
            // 
            // CmbUsuarios
            // 
            this.CmbUsuarios.Font = new System.Drawing.Font("Arial", 18F);
            this.CmbUsuarios.FormattingEnabled = true;
            this.CmbUsuarios.Location = new System.Drawing.Point(13, 50);
            this.CmbUsuarios.Name = "CmbUsuarios";
            this.CmbUsuarios.Size = new System.Drawing.Size(317, 35);
            this.CmbUsuarios.TabIndex = 18;
            // 
            // frmHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1073, 711);
            this.ContextMenuStrip = this.menuHome;
            this.Controls.Add(this.panelTecnico);
            this.Controls.Add(this.panelEmpleado);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblIp);
            this.Controls.Add(this.lblNombreEmpleado);
            this.Controls.Add(this.lblNumeroEmpleado);
            this.Controls.Add(this.lbl03);
            this.Controls.Add(this.lbl04);
            this.Controls.Add(this.lblEquipo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmHome";
            this.Load += new System.EventHandler(this.frmHome_Load);
            this.Resize += new System.EventHandler(this.frmHome_Resize);
            this.tabControl1.ResumeLayout(false);
            this.panelPercance.ResumeLayout(false);
            this.panelPercance.PerformLayout();
            this.panelPendientes.ResumeLayout(false);
            this.panelFlujo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuContextual.ResumeLayout(false);
            this.panelEmpleado.ResumeLayout(false);
            this.panelEmpleado.PerformLayout();
            this.menuHome.ResumeLayout(false);
            this.panelTecnico.ResumeLayout(false);
            this.panelTecnico.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage panelPercance;
        private System.Windows.Forms.TabPage panelPendientes;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblIp;
        private System.Windows.Forms.Label lblEquipo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.RichTextBox txtPercanceDetalle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.FlowLayoutPanel panelFlujo;
        private System.Windows.Forms.Label lblPercanceModelo;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox txtPercanceMotivo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Timer ubicacion;
        private System.Windows.Forms.NotifyIcon iconizarApp;
        private System.Windows.Forms.ContextMenuStrip menuContextual;
        private System.Windows.Forms.ToolStripMenuItem mostrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cerrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem minimizarToolStripMenuItem;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbl04;
        private System.Windows.Forms.Label lbl03;
        private System.Windows.Forms.Label lblNumeroEmpleado;
        private System.Windows.Forms.Label lblNombreEmpleado;
        private System.Windows.Forms.Panel panelEmpleado;
        private System.Windows.Forms.Button btnValidarEmpleado;
        private System.Windows.Forms.TextBox txtNumeroEmpleado;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ContextMenuStrip menuHome;
        private System.Windows.Forms.ToolStripMenuItem itemTecnico;
        private System.Windows.Forms.ToolStripMenuItem itemMinimizar;
        private System.Windows.Forms.Panel panelTecnico;
        private System.Windows.Forms.Button btnValidarTecnico;
        private System.Windows.Forms.TextBox txtPassTecnico;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox CmbUsuarios;
    }
}