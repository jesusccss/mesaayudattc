﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MesaAyudaTTC
{
    public partial class frmSelector : Form{

        public frmSelector(){
            InitializeComponent();
        }// end inicializar


        private void frmSelector_Load(object sender, EventArgs e){
            picCliente.Image = listaImgSelector.Images[0]; // cliente
            picTenico.Image = listaImgSelector.Images[1]; // tecnico

        }// end load


    }// end class
}// end namespace
