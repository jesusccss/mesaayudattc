﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using logica;
using Entidad;

namespace MesaAyudaTTC
{
    public partial class frmHome : Form
    {
        public frmHome(){
            InitializeComponent();
        }// end inicio

        private string Ref_Numeros = "0123456789.";
        private string Ref_Letras = "qazwsxedcrfvtgbyhnujmikolpñQAZWSXEDCRFVTGBYHNUJMIKOLPÑ";

        //ancho: 360
        // alto: 750

        private string[,] Palabras = {
                 {"impresora","Codigo impresora" }
                ,{"contraseña","codigo pass" }
                ,{"pass","codigo " }
                ,{"usuario","codigo " }
                ,{"me registro","codigo " }
        };

        public void Analizar_percance()
        {

            // analizar contenido del percance
            string texto = txtPercanceDetalle.Text;
            string resultado = "";

            if (Text.Length > 0)
            {
                MessageBox.Show(texto, "Percance");
            }


        }// end analizar percance


        private void richTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (txtPercanceDetalle.Text.Length > 0)
            {
                btnRegistrar.Enabled = true;


            }
            else
            {

                btnRegistrar.Enabled = false;
            }

            if (e.KeyChar == ((char)13))
            {
                if (btnRegistrar.Enabled)
                {
                    //Analizar_percance();
                }
            }
            else
            {

            }// end checar enter

            //alerta.Text = (e.KeyChar == ((char)13) ? "[SI]" : "[NO]");



        }// end keypress richtextbox percance



        public void Informacion_Host()
        {
            string host = System.Net.Dns.GetHostName();
            string ip = "";
            System.Net.IPAddress[] hostIPs = System.Net.Dns.GetHostAddresses(host);
            for (int i = 0; i < hostIPs.Length; i++)
            {
                hostIPs[i].ToString().Contains(".");
                if (hostIPs[i].ToString().Contains(".")) ip = hostIPs[i].ToString();
            }
            lblEquipo.Text = host;
            lblIp.Text = ip;

        }// end informacion equipo local



        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e){

            //if (tabControl1.SelectedTab .Equals())
            //{
            //    tabControl1.Show();

            //}
            //else if (tabControl1.SelectedPage.Equals(panelInstt))
            //{
            //    ventanaInstt.Show();

            //}

        }// end cambio de pestaña

        private void frmHome_Load(object sender, EventArgs e){
            Colocar_Paneles();
            PanelTecnico_Desaparece();
            PanelEmpleado_Aparece();

            string usuarios = "";
            ControlXML documento = new ControlXML();
            usuarios = documento.leerUsuariosXML(); // extraigo los usuarios del equipo local
            char diag = '/', separaParam = '$';
            string[] lista = usuarios.Split(diag),param;
            string n = "";
            
            foreach (string s in lista) {
                //MessageBox.Show(s.ToString());
                if (s.Contains("+")) {/*no hacer nada, elemento vacio*/ }
                else {// elemento no vacio
                    param = s.Split( separaParam );// separo los parametros
                    foreach (string ss in param) {
                        if (ss.Contains("#:")) {
                            n = ss.Split((char)':')[1].ToString();
                            CmbUsuarios.Items.Add(n);
                        }
                    }

                    //if (param[0].Contains("#:"))
                    //{
                    //}
                    //else if (param[0].Contains("N:"))
                    //{

                    //}

                } 
                
                
                //if (s.Contains("#:")){
                //}else if (s.Contains("N:")){

                //}

                }// end recorre usuarios





                Informacion_Host();// informacion obtenida del equipo
            //PanelEmpleado_Aparece();

            this.Width = 360; //  ancho del formulario

        }// end load

        private void ubicacion_Tick(object sender, EventArgs e)
        {
            // ubicar la ventana a la derecha y al centro
            this.Left = Screen.AllScreens[0].Bounds.Width - this.Width - 0; // ubica la ventana a la derecha de la pantalla
            this.Top = (Screen.AllScreens[0].Bounds.Height - this.Height) / 2; // centrar en vertical
        }// end ubicacion_Tick



        private void iconizarApp_BalloonTipClicked(object sender, EventArgs e)
        {
            //MessageBox.Show("Contenido", "Titulo");
            maximizar_app();
        }




        public void maximizar_app()
        {
            this.Show();
            WindowState = FormWindowState.Normal;
            Activate();
            iconizarApp.Visible = false;
        }// end maximizar




        public void minimizar_app()
        {
            iconizarApp.Icon = this.Icon;
            iconizarApp.ContextMenuStrip = this.menuContextual;
            iconizarApp.Text = Application.ProductName;
            iconizarApp.Visible = true;
            this.Visible = false;

            //// redimencionar
            //if (this.WindowState == FormWindowState.Minimized)
            //{
            //    //btMinimizar_Click(sender, e);
            //    minimizar_app();
            //    iconizarApp.BalloonTipIcon = ToolTipIcon.Info;
            //    iconizarApp.BalloonTipTitle = "Mesa de Ayuda";//Application.ProductName;
            //    iconizarApp.BalloonTipText = "La aplicación ha quedado oculta " +
            //                                    "en el área de notificación. Para mostrarla haga " +
            //                                    "doble clic sobre el icono";
            //    iconizarApp.ShowBalloonTip(8);// tiempo que sera visible el mensaje

            //    iconizarApp.ShowBalloonTip(20, "Title", "Text", ToolTipIcon.None);

            //}


        }// end minimizar

        private void minimizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            iconizarApp.Icon = this.Icon;
            iconizarApp.ContextMenuStrip = this.menuContextual;
            iconizarApp.Text = Application.ProductName;
            iconizarApp.Visible = true;
            this.Visible = false;
        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e){
            // cerrar aplicacion
            //Application.ExitThread();
            //Application.Exit();


        }// end cerrarToolStripMenuItem_Click

        private void mostrarToolStripMenuItem_Click(object sender, EventArgs e){
            maximizar_app();
        }// end mostrarToolStripMenuItem_Click

        private void minimizarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            iconizarApp.Icon = this.Icon;
            iconizarApp.ContextMenuStrip = this.menuContextual;
            iconizarApp.Text = Application.ProductName;
            iconizarApp.Visible = true;
            this.Visible = false;
        }// end minimizarToolStripMenuItem_Click_1

        private void frmHome_Resize(object sender, EventArgs e){
            // redimencionar
            if (this.WindowState == FormWindowState.Minimized)
            {
                //btMinimizar_Click(sender, e);
                minimizar_app();
                iconizarApp.BalloonTipIcon = ToolTipIcon.Info;
                iconizarApp.BalloonTipTitle = "Mesa de Ayuda";//Application.ProductName;
                iconizarApp.BalloonTipText = "La aplicación ha quedado oculta " +
                                                "en el área de notificación. Para mostrarla haga " +
                                                "doble clic sobre el icono";
                iconizarApp.ShowBalloonTip(4);// tiempo que sera visible el mensaje

                //iconizarApp.ShowBalloonTip(20, "Title", "Text", ToolTipIcon.None); // accion resumida

            }
        }// end frmHome_Resize

        private void iconizarApp_BalloonTipClicked_1(object sender, EventArgs e){
            maximizar_app();
        }// end iconizarApp_BalloonTipClicked_1

        private void btnIngresarEmpleado_Click(object sender, EventArgs e){
            Evento_validar_empleado();
        }// end boton ingresar


        public void Evento_validar_empleado(){
            
            if (txtNumeroEmpleado.Text.Trim().Length > 3){

                LogEmpleado logica;
                EntEmpleado empleado;
                char diagonal = '/',valor=':';
                string    datos = "";
                        //nombre = "",
                        //numero = "";


                empleado = new EntEmpleado(txtNumeroEmpleado.Text);
                logica = new LogEmpleado();
                datos = logica.ValidarEmpleado(empleado.NumeroEmpleado);
                if (datos.Contains("[Y]")){

                    try{string[] dd = datos.Split(diagonal);
                        // recorrer elementos enviados por la base
                        for (int i = 0; i < dd.Length; i++){
                            if (dd[i].Contains("N:")){
                                lblNombreEmpleado.Text  = dd[i].Split(valor)[1].ToString();// asigno el nombre 
                            }else if (dd[i].Contains("E:")){
                                lblNumeroEmpleado.Text  = dd[i].Split(valor)[1].ToString();// asigno el numero 
                            }
                        }// end credenciales
                    }catch (Exception ex){
                        lblNumeroEmpleado.Text = "####";
                        lblNombreEmpleado.Text = "XXXX";
                    }


                    PanelEmpleado_Desaparece();// oculto el panel para usar la GUI
                }
                else {

                    PanelEmpleado_Aparece(); // muestra de nuevo el panel del empleado

                }





                //if (datos.Contains("[Y]")){
                //    PanelEmpleado_Desaparece();
                //}
                //else
                //{
                //    //PanelEmpleado_Aparece();
                //    return false;

                //}// end control de panel empleado


                //string datos = "";
                //if (ValidarEmpleado()){

                //    PanelEmpleado_Desaparece(); // ocultar el panel de registro
                //    txtNumeroEmpleado.ResetText();// limpiar caja


                //    // mostrar datos al cliente
                //    string nombre="", Numero=""
                //    lblNumeroEmpleado.Text = txtNumeroEmpleado.Text;




                //}
                //else
                //{
                //    txtNumeroEmpleado.ResetText();
                //    txtNumeroEmpleado.Focus();

                //    MessageBox.Show("No encontrado", "MesaAyudaTTC", MessageBoxButtons.OK);
                //    // Es necesario el registro del empleado
                //}

            }else {
                // no hay suficientes digitos
            }

            

        }// end Evento_validar_empleado

        public void Colocar_Paneles() {
            // panel empleado
                panelEmpleado.Left = 2;
                panelEmpleado.Top  = 145;
            // panel tecnico
                panelTecnico.Left = 2;
                panelTecnico.Top = 145;
        }// end Colocar_Paneles



        public void PanelEmpleado_Desaparece(){

            panelEmpleado.Visible = false;
            lbl03.Visible = true;
            lbl04.Visible = true;

            lblNumeroEmpleado.Visible = true;
            lblNombreEmpleado.Visible = true;

            txtNumeroEmpleado.ResetText(); // resetear cuadro de texto

        }// end PanelEmpleado_Ocultar
        public void PanelEmpleado_Aparece(){

            panelEmpleado.Visible = true;
            lbl03.Visible = false;
            lbl04.Visible = false;

            lblNumeroEmpleado.Visible = false;
            lblNombreEmpleado.Visible = false;


        }// end PanelEmpleado_Ocultar






        public void PanelTecnico_Desaparece()
        {

            panelTecnico.Visible = false;
            //lbl03.Visible = true;
            //lbl04.Visible = true;

            //lblNumeroEmpleado.Visible = true;
            //lblNombreEmpleado.Visible = true;

            //txtPassTecnico.ResetText(); // resetear cuadro

        }// end PanelEmpleado_Ocultar
        public void PanelTecnico_Aparece()
        {

            panelTecnico.Visible = true;
            //lbl03.Visible = false;
            //lbl04.Visible = false;

            //lblNumeroEmpleado.Visible = false;
            //lblNombreEmpleado.Visible = false;


        }// end PanelEmpleado_Ocultar







        public Boolean ValidarEmpleado(){

            LogEmpleado logica;
            EntEmpleado empleado;
            string resultado = "";
            empleado = new EntEmpleado(txtNumeroEmpleado.Text);
            logica = new LogEmpleado();
            resultado = logica.ValidarEmpleado(empleado.NumeroEmpleado);
            if (resultado.Contains("[Y]"))
            {
                //PanelEmpleado_Desaparece();
                return true;
            }
            else
            {
                //PanelEmpleado_Aparece();
                return false;

            }// end control de panel empleado




          

        }// end ValidarEmpleado
        public Boolean ValidarTecnico(){

            LogEmpleado logica;
            EntEmpleado empleado;
            string resultado = "";
            empleado = new EntEmpleado(txtNumeroEmpleado.Text);
            logica = new LogEmpleado();
            resultado = logica.ValidarTecnico( lblNumeroEmpleado.Text , txtPassTecnico.Text );
            if (resultado.Contains("[Y]"))
            {
                //PanelEmpleado_Desaparece();
                return true;
            }
            else
            {
                //PanelEmpleado_Aparece();
                return false;

            }// end control de panel empleado




            

        }// end ValidarTecnico




        private void itemMinimizar_Click(object sender, EventArgs e){
            minimizar_app();
        }// end 

        private void txtNumeroEmpleado_KeyPress(object sender, KeyPressEventArgs e){


            if (e.KeyChar == (char)Keys.Enter) {
                Evento_validar_empleado();
            }else if(e.KeyChar == (char)Keys.Tab){
                btnValidarEmpleado.Enabled = true;
                btnValidarEmpleado.Focus();
            }else{
                // solo numeros
                if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
                {
                    
                    e.Handled = true;
                    return;
                }

                //if (char.IsNumber(e.KeyChar)){
                //    // caracter permitido
                //    e.Handled = false;
                //}
                //else {

                //}

            }



            //if (e.KeyChar != (char)Keys.Back) {

            //    if(e.KeyChar != (char)Keys.Space  )

            //}

            //if (!(char.IsLetter(e.KeyChar))) {

            //}

            //// validar numeros 
            //if (!Ref_Numeros.Contains(e.KeyChar)) { e.KeyChar = ; }

            //// e.KeyChar
            //btnValidarEmpleado.Enabled = (txtNumeroEmpleado.Text.Trim().Length == 0);

        }// end txtNumeroEmpleado_KeyPress

        private void itemTecnico_Click(object sender, EventArgs e){

            txtPassTecnico.ResetText();
            PanelTecnico_Aparece();

            PanelEmpleado_Desaparece();

        }// end itemTecnico_Click

        private void txtPassTecnico_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtNumeroEmpleado_TextChanged(object sender, EventArgs e)
        {

            if (txtNumeroEmpleado.Text.Trim().Length >= 4) { btnValidarEmpleado.Enabled = true; }
            else { btnValidarEmpleado.Enabled = false; }
        }


    }// end class
}// end namespace
