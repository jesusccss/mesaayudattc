﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MesaAyudaTTC{
    public partial class frmActividadNueva : Form{
        public frmActividadNueva(){
            InitializeComponent();
        }// end inicializar

        //private int PosX;
        //private int PosY;


        private string[,] Palabras = {
                 {"impresora","Codigo impresora" }
                ,{"contraseña","codigo pass" }
                ,{"pass","codigo " }
                ,{"usuario","codigo " }
                ,{"me registro","codigo " }
        };


        private void Form1_Load(object sender, EventArgs e){
            Informacion_Host();
        }// end load

        private void ubicacion_Tick(object sender, EventArgs e){
            // ubicar la ventana a la derecha y al centro
            this.Left = Screen.AllScreens[0].Bounds.Width - this.Width - 0; // ubica la ventana a la derecha de la pantalla
            this.Top = (Screen.AllScreens[0].Bounds.Height - this.Height) / 2; // centrar en vertical
        }// end timer ubicacion

        public void Informacion_Host() {
            string host = System.Net.Dns.GetHostName();
            string ip = "";
            System.Net.IPAddress[] hostIPs = System.Net.Dns.GetHostAddresses(host);
            for (int i = 0; i < hostIPs.Length; i++){
                hostIPs[i].ToString().Contains(".");
                if (hostIPs[i].ToString().Contains(".")) ip = hostIPs[i].ToString();
            }
            lblEquipo.Text = host;
            lblIp.Text = ip;

        }// end informacion equipo local

        private void btnRegistrar_Click(object sender, EventArgs e){


        }// end notificar percance

        public void Analizar_percance(){

            // analizar contenido del percance
            string texto = txtPercance.Text;
            string resultado = "";

            if (Text.Length > 0)
            {
                MessageBox.Show(texto, "Percance");
            }


        }// end analizar percance

        private void richTextBox1_KeyPress(object sender, KeyPressEventArgs e){

            if (txtPercance.Text.Length > 0)
            {
                btnRegistrar.Enabled = true;


            }
            else {

                btnRegistrar.Enabled = false;
            }

            if (e.KeyChar == ((char)13))
            {
                if (btnRegistrar.Enabled)
                {
                    //Analizar_percance();
                }
            }
            else {

            }// end checar enter

            alerta.Text = (e.KeyChar == ((char)13)?"[SI]":"[NO]");



        }// end keypress richtextbox percance

        private void menuitemAbrir_Click(object sender, EventArgs e)
        {
            maximizar_app();
        }

        private void menuItemCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmActividadNueva_Resize(object sender, EventArgs e)
        {
            // 
            if (this.WindowState == FormWindowState.Minimized)
            {
                //btMinimizar_Click(sender, e);
                minimizar_app();
                iconizarApp.BalloonTipIcon = ToolTipIcon.Info;
                iconizarApp.BalloonTipTitle = Application.ProductName;
                iconizarApp.BalloonTipText = "La aplicación ha quedado ocultada " +
                    "en el área de notificación. Para mostrarla haga " +
                    "doble clic sobre el icono";
                iconizarApp.ShowBalloonTip(8);
            }
        }

        private void iconizarApp_BalloonTipClicked(object sender, EventArgs e)
        {
            //MessageBox.Show("Contenido", "Titulo");
            maximizar_app();
        }




        public void maximizar_app() {
            this.Show();
            WindowState = FormWindowState.Normal;
            Activate();
            iconizarApp.Visible = false;
        }// end maximizar




        public void minimizar_app() {
            iconizarApp.Icon = this.Icon;
            iconizarApp.ContextMenuStrip = this.menuContextual;
            iconizarApp.Text = Application.ProductName;
            iconizarApp.Visible = true;
            this.Visible = false;
        }// end minimizar

        private void minimizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            iconizarApp.Icon = this.Icon;
            iconizarApp.ContextMenuStrip = this.menuContextual;
            iconizarApp.Text = Application.ProductName;
            iconizarApp.Visible = true;
            this.Visible = false;
        }
    }// end class
}// end namespace
