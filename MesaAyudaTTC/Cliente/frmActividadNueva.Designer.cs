﻿namespace MesaAyudaTTC
{
    partial class frmActividadNueva
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmActividadNueva));
            this.ubicacion = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblEquipo = new System.Windows.Forms.Label();
            this.lblIp = new System.Windows.Forms.Label();
            this.txtPercance = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.iconizarApp = new System.Windows.Forms.NotifyIcon(this.components);
            this.menuContextual = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuItemAbrir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemCerrar = new System.Windows.Forms.ToolStripMenuItem();
            this.minimizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alerta = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.menuContextual.SuspendLayout();
            this.SuspendLayout();
            // 
            // ubicacion
            // 
            this.ubicacion.Enabled = true;
            this.ubicacion.Tick += new System.EventHandler(this.ubicacion_Tick);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            resources.ApplyResources(this.pictureBox2, "pictureBox2");
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // lblEquipo
            // 
            resources.ApplyResources(this.lblEquipo, "lblEquipo");
            this.lblEquipo.Name = "lblEquipo";
            // 
            // lblIp
            // 
            resources.ApplyResources(this.lblIp, "lblIp");
            this.lblIp.Name = "lblIp";
            // 
            // txtPercance
            // 
            this.txtPercance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtPercance, "txtPercance");
            this.txtPercance.Name = "txtPercance";
            this.txtPercance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.richTextBox1_KeyPress);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // btnRegistrar
            // 
            resources.ApplyResources(this.btnRegistrar, "btnRegistrar");
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // iconizarApp
            // 
            this.iconizarApp.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            resources.ApplyResources(this.iconizarApp, "iconizarApp");
            this.iconizarApp.ContextMenuStrip = this.menuContextual;
            this.iconizarApp.BalloonTipClicked += new System.EventHandler(this.iconizarApp_BalloonTipClicked);
            // 
            // menuContextual
            // 
            this.menuContextual.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemAbrir,
            this.menuItemCerrar,
            this.minimizarToolStripMenuItem});
            this.menuContextual.Name = "menuContextual";
            resources.ApplyResources(this.menuContextual, "menuContextual");
            // 
            // menuItemAbrir
            // 
            this.menuItemAbrir.Name = "menuItemAbrir";
            resources.ApplyResources(this.menuItemAbrir, "menuItemAbrir");
            this.menuItemAbrir.Click += new System.EventHandler(this.menuitemAbrir_Click);
            // 
            // menuItemCerrar
            // 
            this.menuItemCerrar.Name = "menuItemCerrar";
            resources.ApplyResources(this.menuItemCerrar, "menuItemCerrar");
            this.menuItemCerrar.Click += new System.EventHandler(this.menuItemCerrar_Click);
            // 
            // minimizarToolStripMenuItem
            // 
            this.minimizarToolStripMenuItem.Name = "minimizarToolStripMenuItem";
            resources.ApplyResources(this.minimizarToolStripMenuItem, "minimizarToolStripMenuItem");
            this.minimizarToolStripMenuItem.Click += new System.EventHandler(this.minimizarToolStripMenuItem_Click);
            // 
            // alerta
            // 
            resources.ApplyResources(this.alerta, "alerta");
            this.alerta.Name = "alerta";
            // 
            // frmActividadNueva
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btnRegistrar);
            this.Controls.Add(this.txtPercance);
            this.Controls.Add(this.lblIp);
            this.Controls.Add(this.lblEquipo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.alerta);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmActividadNueva";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.frmActividadNueva_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.menuContextual.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer ubicacion;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblEquipo;
        private System.Windows.Forms.Label lblIp;
        private System.Windows.Forms.RichTextBox txtPercance;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NotifyIcon iconizarApp;
        private System.Windows.Forms.ContextMenuStrip menuContextual;
        private System.Windows.Forms.ToolStripMenuItem menuItemAbrir;
        private System.Windows.Forms.ToolStripMenuItem menuItemCerrar;
        private System.Windows.Forms.ToolStripMenuItem minimizarToolStripMenuItem;
        private System.Windows.Forms.Label alerta;
    }
}

