﻿namespace MesaAyudaTTC
{
    partial class frmSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSelector));
            this.picCliente = new System.Windows.Forms.PictureBox();
            this.picTenico = new System.Windows.Forms.PictureBox();
            this.listaImgBotonCerrar = new System.Windows.Forms.ImageList(this.components);
            this.btnCerrar = new System.Windows.Forms.PictureBox();
            this.listaImgSelector = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTenico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).BeginInit();
            this.SuspendLayout();
            // 
            // picCliente
            // 
            this.picCliente.Location = new System.Drawing.Point(21, 6);
            this.picCliente.Name = "picCliente";
            this.picCliente.Size = new System.Drawing.Size(80, 80);
            this.picCliente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCliente.TabIndex = 0;
            this.picCliente.TabStop = false;
            // 
            // picTenico
            // 
            this.picTenico.Location = new System.Drawing.Point(122, 6);
            this.picTenico.Name = "picTenico";
            this.picTenico.Size = new System.Drawing.Size(80, 80);
            this.picTenico.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picTenico.TabIndex = 0;
            this.picTenico.TabStop = false;
            // 
            // listaImgBotonCerrar
            // 
            this.listaImgBotonCerrar.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("listaImgBotonCerrar.ImageStream")));
            this.listaImgBotonCerrar.TransparentColor = System.Drawing.Color.Transparent;
            this.listaImgBotonCerrar.Images.SetKeyName(0, "close_blanco.bmp");
            this.listaImgBotonCerrar.Images.SetKeyName(1, "close_negro.bmp");
            // 
            // btnCerrar
            // 
            this.btnCerrar.Location = new System.Drawing.Point(474, 6);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(10, 10);
            this.btnCerrar.TabIndex = 0;
            this.btnCerrar.TabStop = false;
            // 
            // listaImgSelector
            // 
            this.listaImgSelector.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("listaImgSelector.ImageStream")));
            this.listaImgSelector.TransparentColor = System.Drawing.Color.Transparent;
            this.listaImgSelector.Images.SetKeyName(0, "userconfig.png");
            this.listaImgSelector.Images.SetKeyName(1, "Businessman.png");
            // 
            // frmSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 94);
            this.ControlBox = false;
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.picTenico);
            this.Controls.Add(this.picCliente);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmSelector";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmSelector_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTenico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picCliente;
        private System.Windows.Forms.PictureBox picTenico;
        private System.Windows.Forms.ImageList listaImgBotonCerrar;
        private System.Windows.Forms.PictureBox btnCerrar;
        private System.Windows.Forms.ImageList listaImgSelector;
    }
}