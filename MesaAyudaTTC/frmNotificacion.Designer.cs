﻿namespace MesaAyudaTTC
{
    partial class frmNotificacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNotificacion));
            this.picImagen = new System.Windows.Forms.PictureBox();
            this.btnCerrar_notificacion = new System.Windows.Forms.PictureBox();
            this.txtMensaje = new System.Windows.Forms.RichTextBox();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.almacen_img = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picImagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar_notificacion)).BeginInit();
            this.SuspendLayout();
            // 
            // picImagen
            // 
            this.picImagen.Image = ((System.Drawing.Image)(resources.GetObject("picImagen.Image")));
            this.picImagen.Location = new System.Drawing.Point(0, 0);
            this.picImagen.Name = "picImagen";
            this.picImagen.Size = new System.Drawing.Size(100, 99);
            this.picImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picImagen.TabIndex = 0;
            this.picImagen.TabStop = false;
            // 
            // btnCerrar_notificacion
            // 
            this.btnCerrar_notificacion.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrar_notificacion.Image")));
            this.btnCerrar_notificacion.Location = new System.Drawing.Point(476, 6);
            this.btnCerrar_notificacion.Name = "btnCerrar_notificacion";
            this.btnCerrar_notificacion.Size = new System.Drawing.Size(17, 18);
            this.btnCerrar_notificacion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnCerrar_notificacion.TabIndex = 0;
            this.btnCerrar_notificacion.TabStop = false;
            this.btnCerrar_notificacion.Click += new System.EventHandler(this.btnCerrar_notificacion_Click);
            this.btnCerrar_notificacion.MouseEnter += new System.EventHandler(this.btnCerrar_notificacion_MouseEnter);
            this.btnCerrar_notificacion.MouseLeave += new System.EventHandler(this.btnCerrar_notificacion_MouseLeave);
            this.btnCerrar_notificacion.MouseHover += new System.EventHandler(this.btnCerrar_notificacion_MouseHover);
            // 
            // txtMensaje
            // 
            this.txtMensaje.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMensaje.Location = new System.Drawing.Point(106, 24);
            this.txtMensaje.Name = "txtMensaje";
            this.txtMensaje.Size = new System.Drawing.Size(387, 75);
            this.txtMensaje.TabIndex = 1;
            this.txtMensaje.Text = "";
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(107, 3);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(123, 19);
            this.lblTitulo.TabIndex = 2;
            this.lblTitulo.Text = "Mesa de ayuda";
            // 
            // almacen_img
            // 
            this.almacen_img.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("almacen_img.ImageStream")));
            this.almacen_img.TransparentColor = System.Drawing.Color.Transparent;
            this.almacen_img.Images.SetKeyName(0, "close_negro.bmp");
            this.almacen_img.Images.SetKeyName(1, "close_blanco.bmp");
            this.almacen_img.Images.SetKeyName(2, "close_negro.png");
            this.almacen_img.Images.SetKeyName(3, "close_blanco.png");
            // 
            // frmNotificacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(500, 100);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.txtMensaje);
            this.Controls.Add(this.btnCerrar_notificacion);
            this.Controls.Add(this.picImagen);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmNotificacion";
            this.Text = "frmNotificacion";
            this.TransparencyKey = System.Drawing.Color.Maroon;
            this.Load += new System.EventHandler(this.frmNotificacion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picImagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCerrar_notificacion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox picImagen;
        private System.Windows.Forms.PictureBox btnCerrar_notificacion;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.RichTextBox txtMensaje;
        private System.Windows.Forms.ImageList almacen_img;
    }
}