﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad{

    public class EntEmpleado{

        private string _NumeroEmpleado;
        private string _Nombre;
        private string _App;
        private string _Apm;

        private string _CURP;
        private string _RFC;

        private string _EMAIL_PERSONAL;
        private string _EMAIL_LABORAL;


        private string _CELULAR;
        private string _TELEFONO;

        private string _Departamento;
        private string _ROL;

        private string _PASS;// pass solo para los tecnicos


        public EntEmpleado(string NumeroEmpleado, string Nombre, string App, string Apm, string CURP, string RFC, string EMAIL_PERSONAL, string EMAIL_LABORAL, string CELULAR, string TELEFONO, string Departamento, string ROL)
        {
            this._NumeroEmpleado = NumeroEmpleado;
            this._Nombre = Nombre;
            this._App = App;
            this._Apm = Apm;
            this._CURP = CURP;
            this._RFC = RFC;
            this._EMAIL_PERSONAL = EMAIL_PERSONAL;
            this._EMAIL_LABORAL = EMAIL_LABORAL;
            this._CELULAR = CELULAR;
            this._TELEFONO = TELEFONO;
            this._Departamento = Departamento;
            this._ROL = ROL;
        }

        public EntEmpleado(string  NumeroEmpleado)
        {
            this._NumeroEmpleado = NumeroEmpleado;
        }

        public string NumeroEmpleado
        {
            get
            {
                return _NumeroEmpleado;
            }

            set
            {
                _NumeroEmpleado = value;
            }
        }

        public string Nombre
        {
            get
            {
                return _Nombre;
            }

            set
            {
                _Nombre = value;
            }
        }

        public string App
        {
            get
            {
                return _App;
            }

            set
            {
                _App = value;
            }
        }

        public string Apm
        {
            get
            {
                return _Apm;
            }

            set
            {
                _Apm = value;
            }
        }

        public string CURP
        {
            get
            {
                return _CURP;
            }

            set
            {
                _CURP = value;
            }
        }

        public string RFC
        {
            get
            {
                return _RFC;
            }

            set
            {
                _RFC = value;
            }
        }

        public string EMAIL_PERSONAL
        {
            get
            {
                return _EMAIL_PERSONAL;
            }

            set
            {
                _EMAIL_PERSONAL = value;
            }
        }

        public string EMAIL_LABORAL
        {
            get
            {
                return _EMAIL_LABORAL;
            }

            set
            {
                _EMAIL_LABORAL = value;
            }
        }

        public string CELULAR
        {
            get
            {
                return _CELULAR;
            }

            set
            {
                _CELULAR = value;
            }
        }

        public string TELEFONO
        {
            get
            {
                return _TELEFONO;
            }

            set
            {
                _TELEFONO = value;
            }
        }

        public string Departamento
        {
            get
            {
                return _Departamento;
            }

            set
            {
                _Departamento = value;
            }
        }

        public string ROL
        {
            get
            {
                return _ROL;
            }

            set
            {
                _ROL = value;
            }
        }

        public string PASS
        {
            get
            {
                return _PASS;
            }

            set
            {
                _PASS = value;
            }
        }
    }// end 
}// end namespace
