﻿using System;
using System.Data.SqlClient;
using System.Data;
using AccesoDatos;
using System.Configuration;
using Entidad;
using System.Windows.Forms;


namespace logica{

    public class LogEmpleado{
        public string ValidarEmpleado(string NumeroEmpleado)
        {
            string msg = "[Error logValidarEmpleado]";
            string cadenaConexion = "";
            try
            {
                cadenaConexion = ConfigurationManager.AppSettings.Get("liga");
                Acceso ac = new Acceso();
                SqlConnection cnn = new SqlConnection(cadenaConexion);
                cnn.Open();
                SqlCommand alta = new SqlCommand("spuValidarEmpleado", cnn);
                alta.CommandType = CommandType.StoredProcedure;
                alta.Parameters.Add("@empleado", SqlDbType.NVarChar, 50).Value = NumeroEmpleado;
                alta.Parameters.Add("@msg", SqlDbType.NVarChar, 400).Direction = ParameterDirection.Output;
                alta.ExecuteNonQuery();
                cnn.Close();
                return alta.Parameters["@msg"].Value.ToString(); // checo si es un empleado
            }
            catch (Exception ex)
            {
                return "[N]";
            }
        }// end ValidarEmpleado

        public string ValidarTecnico(string NumeroEmpleado, string pass)
        {
            string msg = "[Error logValidarTecnico]";
            string cadenaConexion = "";
            try
            {
                cadenaConexion = ConfigurationManager.AppSettings.Get("liga");
                Acceso ac = new Acceso();
                SqlConnection cnn = new SqlConnection(cadenaConexion);
                cnn.Open();
                SqlCommand alta = new SqlCommand("spuValidarTecnico", cnn);
                alta.CommandType = CommandType.StoredProcedure;
                alta.Parameters.Add("@tecnico", SqlDbType.NVarChar, 50).Value = NumeroEmpleado;
                alta.Parameters.Add("@msg", SqlDbType.NVarChar, 400).Direction = ParameterDirection.Output;
                alta.ExecuteNonQuery();
                cnn.Close();
                return alta.Parameters["@msg"].Value.ToString(); // checo si es un Tecnico
            }
            catch (Exception ex)
            {
                return "[N]";
            }
        }// end ValidarTecnico


        public void DeshabilitarEmpleado(string NumeroEmpleado)
        {
            string msg = "[Error logValidarTecnico]";
            string cadenaConexion = "";
            try
            {
                cadenaConexion = ConfigurationManager.AppSettings.Get("liga");
                Acceso ac = new Acceso();
                SqlConnection cnn = new SqlConnection(cadenaConexion);
                cnn.Open();
                SqlCommand alta = new SqlCommand("spuEmpleadoDelete", cnn);
                alta.CommandType = CommandType.StoredProcedure;
                alta.Parameters.Add("@numero", SqlDbType.NVarChar, 50).Value = NumeroEmpleado;
                alta.Parameters.Add("@msg", SqlDbType.NVarChar, 400).Direction = ParameterDirection.Output;
                alta.ExecuteNonQuery();
                cnn.Close();
                //return alta.Parameters["@msg"].Value.ToString().Contains("[Y]"); // checo si es un empleado
            }
            catch (Exception ex)
            {
                //return false;
            }
        }// end ValidarTecnico


        public void RegistrarEmpleado(EntEmpleado empleado){
            string msg = "[Error logRegistrarEmpleado]", cadenaConexion = "";
            try{cadenaConexion = ConfigurationManager.AppSettings.Get("liga");
                Acceso ac = new Acceso();
                SqlConnection cnn = new SqlConnection(cadenaConexion);
                cnn.Open();
                SqlCommand alta = new SqlCommand("spuEmpleadoInsert", cnn);
                alta.CommandType = CommandType.StoredProcedure;
                alta.Parameters.Add("@numero", SqlDbType.NVarChar, 200).Value = empleado.NumeroEmpleado;
                alta.Parameters.Add("@nombre", SqlDbType.NVarChar, 200).Value = empleado.Nombre  ;
                alta.Parameters.Add("@apellido_paterno", SqlDbType.NVarChar, 200).Value =  empleado.App ;
                alta.Parameters.Add("@apellido_materno", SqlDbType.NVarChar, 200).Value =  empleado.Apm ;
                alta.Parameters.Add("@curp", SqlDbType.NVarChar, 200).Value =  empleado.CURP ;
                alta.Parameters.Add("@rfc", SqlDbType.NVarChar, 200).Value =  empleado.RFC ;
                alta.Parameters.Add("@personal", SqlDbType.NVarChar, 200).Value =  empleado.EMAIL_PERSONAL ;
                alta.Parameters.Add("@laboral", SqlDbType.NVarChar, 200).Value =  empleado.EMAIL_LABORAL ;
                alta.Parameters.Add("@capturista", SqlDbType.NVarChar, 200).Value =  "System";
                alta.Parameters.Add("@msg", SqlDbType.NVarChar, 400).Direction = ParameterDirection.Output;
                alta.ExecuteNonQuery();
                cnn.Close();
                if (alta.Parameters["@msg"].Value.ToString().Contains("correctamente")){
                    // no hacer nada
                }else {
                    MessageBox.Show(
                                         alta.Parameters["@msg"].Value.ToString()
                                        ,"Informacion " + msg
                                        ,MessageBoxButtons.OK
                                        ,MessageBoxIcon.Information
                                );
                }
            }catch (Exception ex){}
        }// end registrar empleado


        public void UpdateEmpleado(EntEmpleado empleado)
        {
            string msg = "[Error logActualizarEmpleado]", cadenaConexion = "";
            try
            {
                cadenaConexion = ConfigurationManager.AppSettings.Get("liga");
                Acceso ac = new Acceso();
                SqlConnection cnn = new SqlConnection(cadenaConexion);
                cnn.Open();
                SqlCommand alta = new SqlCommand("spuEmpleadoUpdate", cnn);
                alta.CommandType = CommandType.StoredProcedure;
                alta.Parameters.Add("@numero", SqlDbType.NVarChar, 200).Value = empleado.NumeroEmpleado;
                alta.Parameters.Add("@nombre", SqlDbType.NVarChar, 200).Value = empleado.Nombre;
                alta.Parameters.Add("@apellido_paterno", SqlDbType.NVarChar, 200).Value = empleado.App;
                alta.Parameters.Add("@apellido_materno", SqlDbType.NVarChar, 200).Value = empleado.Apm;
                alta.Parameters.Add("@curp", SqlDbType.NVarChar, 200).Value = empleado.CURP;
                alta.Parameters.Add("@rfc", SqlDbType.NVarChar, 200).Value = empleado.RFC;
                alta.Parameters.Add("@personal", SqlDbType.NVarChar, 200).Value = empleado.EMAIL_PERSONAL;
                alta.Parameters.Add("@laboral", SqlDbType.NVarChar, 200).Value = empleado.EMAIL_LABORAL;
                //alta.Parameters.Add("@capturista", SqlDbType.NVarChar, 200).Value = "System";
                alta.Parameters.Add("@msg", SqlDbType.NVarChar, 400).Direction = ParameterDirection.Output;
                alta.ExecuteNonQuery();
                cnn.Close();
                if (alta.Parameters["@msg"].Value.ToString().Contains("correctamente"))
                {
                    // no hacer nada
                }
                else
                {
                    MessageBox.Show(
                                         alta.Parameters["@msg"].Value.ToString()
                                        , "Informacion " + msg
                                        , MessageBoxButtons.OK
                                        , MessageBoxIcon.Information
                                );
                }
            }
            catch (Exception ex) { }
        }// end registrar empleado



    }// class
}//end namespace
